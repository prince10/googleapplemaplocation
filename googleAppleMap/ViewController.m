//
//  ViewController.m
//  googleAppleMap
//
//  Created by Prince on 14/10/15.
//  Copyright (c) 2015 Prince. All rights reserved.
//

#import "ViewController.h"
#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import <GoogleMaps/GoogleMaps.h>
@interface ViewController ()
{
    
    CLLocationManager *manager;
    CLLocation *currentLocation;
    NSString *storedCityName;
    
NSArray *data;
    NSArray *searchData;
    
}
@property (strong, nonatomic) IBOutlet UIView *googleMap;
@property (strong, nonatomic) IBOutlet MKMapView *appleMap;
@property (strong, nonatomic) IBOutlet UISearchBar *searchBar;

@property (strong, nonatomic) IBOutlet UILabel *label1;
@property (strong, nonatomic) IBOutlet UILabel *label2;
@property (strong, nonatomic) IBOutlet UITextField *textField1;
@property (strong, nonatomic) IBOutlet UITextField *textField2;
@property (strong, nonatomic) IBOutlet UIButton *button1;




//@property (strong, nonatomic) IBOutlet UITableView *tableView1;



@end

@implementation ViewController
@synthesize googleMap;
@synthesize appleMap;
//@synthesize tableView1;

- (void)viewDidLoad {
    
    manager=[[CLLocationManager alloc]init];
    manager.delegate=self;
    manager.desiredAccuracy=kCLLocationAccuracyBest;
    [manager requestWhenInUseAuthorization];
    [manager startUpdatingLocation];

    
    
    
    
   
    
  //  data=searchData;
   // [tableView1 reloadData];
   // tableView1.hidden=true;
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}
- (IBAction)getLocationPressed:(id)sender {
    
   [ manager startUpdatingLocation];
    
    
    
}
    - (void)locationManager:(CLLocationManager *)manager
didFailWithError:(NSError *)error
    {
        NSLog(@"didFailWithError: %@", error);
        UIAlertView *errorAlert = [[UIAlertView alloc]
                                   initWithTitle:@"Error" message:@"Failed to Get Your Location" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [errorAlert show];
    }
    - (void)locationManager:(CLLocationManager *)manager
didUpdateToLocation:(CLLocation *)newLocation
fromLocation:(CLLocation *)oldLocation
    {
        NSLog(@"didUpdateToLocation: %@", newLocation);
        currentLocation = newLocation;
        
        if (currentLocation != nil) {
            self.textField1.text = [NSString stringWithFormat:@"%.8f", currentLocation.coordinate.longitude];
            self.textField2.text= [NSString stringWithFormat:@"%.8f", currentLocation.coordinate.latitude];
            
            
            
            CLLocationCoordinate2D position=currentLocation.coordinate;
            MKPointAnnotation *annotationPoint = [[MKPointAnnotation alloc] init];
            MKCoordinateRegion region=MKCoordinateRegionMakeWithDistance(currentLocation.coordinate, 500, 500);
            [self.appleMap setRegion:region animated:YES];            //[self.appleMap setRegion:region];
    
            annotationPoint.coordinate = position;
            annotationPoint.title = [NSString stringWithFormat:@"%@",storedCityName];
            annotationPoint.subtitle = @"sir";
            [appleMap addAnnotation:annotationPoint];
            
            
            
            
            GMSMarker *show = [[GMSMarker alloc] init];
            show.position = currentLocation.coordinate;
            show.title = [NSString stringWithFormat:@"%@",storedCityName];
            show.snippet = @"";
            show.map = googleMap;
            
          
            [self Location];
            
            
    
}
    }

        - (void)Location {
            
            CLGeocoder *geocoder = [[CLGeocoder alloc] init];
            
            CLLocation *newLocation = [[CLLocation alloc]initWithLatitude:currentLocation.coordinate.latitude
                                                                longitude:currentLocation.coordinate.longitude];
            
            [geocoder reverseGeocodeLocation:newLocation
                           completionHandler:^(NSArray *placemarks, NSError *error) {
                               
                               if (error) {
                                   NSLog(@"Geocode failed with error: %@", error);
                                   return;
                               }
                               
                               if (placemarks && placemarks.count > 0)
                               {
                                   CLPlacemark *placemark = placemarks[0];
                                   
                                   NSDictionary *addressDictionary =
                                   placemark.addressDictionary;
                                   NSLog(@"%@",addressDictionary);
                                 
                                   
                                   
                                   storedCityName=addressDictionary[@"City"];
                                   
                               }
                               
                           }];

            
        }



//
//- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
//{
//    return 1;
//}
//- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
//{
//    return 4;
//}
//
//- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    UITableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:@"identity"];
//    //cell.textLabel.text=searchResult[indexPath.row];
//    return cell;
//}
//
//- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
//{
//    NSPredicate *resultPredicate = [NSPredicate predicateWithFormat:@"SELF contains[c] %@", searchText];
//    
//    NSArray *results = [data filteredArrayUsingPredicate:resultPredicate];

    
   // if (searchResult.count==0) {
        
       // searchResult=[NSArray arrayWithArray:data];
        
    
  //  [tableView1 reloadData];
//}

    
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
